import os
from alembic import command
from alembic.config import Config

from app import db

db.Base.metadata.create_all(bind=db.engine)
