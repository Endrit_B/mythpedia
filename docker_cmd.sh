#!/bin/bash
echo "Applying database migrations"
alembic upgrade head

echo "Starting service"
gunicorn app.main:app --bind 0.0.0.0:8000 -w 1 -k uvicorn.workers.UvicornWorker

