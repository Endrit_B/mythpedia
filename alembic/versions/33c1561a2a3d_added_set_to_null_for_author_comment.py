"""added set to null for author-comment

Revision ID: 33c1561a2a3d
Revises: 6cffe2a32c27
Create Date: 2023-04-30 16:45:53.858046

"""
from alembic import op
import sqlalchemy as sa
import sqlalchemy_utils


# revision identifiers, used by Alembic.
revision = '33c1561a2a3d'
down_revision = '6cffe2a32c27'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('comments_author_id_fkey', 'comments', type_='foreignkey')
    op.create_foreign_key(None, 'comments', 'users', ['author_id'], ['id'], ondelete='SET NULL')
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'comments', type_='foreignkey')
    op.create_foreign_key('comments_author_id_fkey', 'comments', 'users', ['author_id'], ['id'])
    # ### end Alembic commands ###
