#!/bin/bash

source ~/mythpedia/.venv/bin/activate
gunicorn app.main:app  --workers 3 --worker-class uvicorn.workers.UvicornWorker --bind 0.0.0.0

