FROM python:3.9.6
ARG DATABASE_URI_
ARG MEDIA_URI_
ARG SECRET_KEY_
ENV DATABASE_URI=$DATABASE_URI_
ENV MEDIA_URI=$MEDIA_URI_
ENV SECRET_KEY=$SECRET_KEY_
ENV ALGORITHM=HS256
ENV ACCESS_TOKEN_EXPIRE_MINUTES=30


COPY ./ /app

WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt

RUN echo "the value pf DATABASE_URI is $DATABASE_URI"
#CMD ["python","app/main.py"]
RUN ["chmod", "+x", "docker_cmd.sh"]
CMD ["./docker_cmd.sh"]

