import uuid

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
import time
from app import crud
from app.db import get_db
from app.schemas import character as SchemaCharacter
from app import models

router = APIRouter()


@router.post("/character", status_code=201, response_model=SchemaCharacter.CharacterWriteWithMyth)
def post_character(character: SchemaCharacter.CharacterWrite, db: Session = Depends(get_db)):
    db_character = crud.get_character_by_name(db=db, name=character.name)
    if db_character:
        raise HTTPException(status_code=409, detail="A character with that name already exists")
    else:
        return crud.post_character(db, character)


@router.get("/get-character", response_model=SchemaCharacter.CharacterWriteWithMyth)
def read_character(character_id: uuid.UUID, db: Session = Depends(get_db)):
    db_character = crud.get_characters(character_id=character_id, db=db)
    if db_character:
        return db_character
    else:
        raise HTTPException(status_code=404, detail="Seems like the character does not exist yet")


@router.delete("/delete-character/{id}", response_model=SchemaCharacter.CharacterDelete)
def delete_character(id: uuid.UUID, db: Session = Depends(get_db)):
    db_character = crud.get_characters(db=db, character_id=id)
    if db_character:
        return crud.delete_character(db, id)
    else:
        raise HTTPException(status_code=404, detail="Character not found")
