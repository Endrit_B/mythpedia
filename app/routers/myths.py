import uuid
from typing import List

from fastapi import APIRouter, Depends, HTTPException

from app import crud, schemas
from app.db import get_db
from app.models.myths import Myths

router = APIRouter()


@router.get("/")
def index():
    return "Hello from index"


@router.post("/post-myth", response_model=schemas.myths.MythWrite)
def post_myth(myth: schemas.MythWrite, db=Depends(get_db)):
    db_myth = crud.get_myth_byt_tittle(db=db, title=myth.title)
    if db_myth:
        raise HTTPException(status_code=409, detail="Myth with that title already exists")
    else:
        return crud.post_myth(db, myth)


@router.get("/get-myth", response_model=schemas.MythWrite)
def get_myth(id: uuid.UUID, db=Depends(get_db)):
    db_myth = crud.get_myth_by_id(db, id)
    if db_myth:
        return db_myth
    else:
        raise HTTPException(status_code=404, detail="A myth by that user_id does not exist")


@router.get("/get-all-myths", response_model=List[schemas.MythWrite])
def get_all_myths(db=Depends(get_db)):
    return crud.get_all_myths(db)


@router.delete("/delete-myth", response_model=schemas.MythWrite)
def delete_myth(myth_id: uuid.UUID, db=Depends(get_db)):
    db_myth = crud.get_myth_by_id(db=db, uuid=myth_id)
    if db_myth:
        return crud.delete_myth(db, myth_id)
    else:
        raise HTTPException(status_code=404, detail="Myth not found")


@router.patch("/update-myth/{myth_id}")
def update_myth(myth_id: uuid.UUID, myth: schemas.MythUpdate, db=Depends(get_db)):
    myth_db = crud.get_myth_by_id(db=db, uuid=myth_id)
    if myth_db:
        return crud.update_myth(myth_id, myth, db)
    else:
        raise HTTPException(status_code=404, detail="Myth not found")
