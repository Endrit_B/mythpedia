import uuid
from fastapi import APIRouter, HTTPException
from fastapi import Depends
from sqlalchemy.orm import Session
from app import crud
from app.db import get_db
from app import schemas
from app import utils
from typing import List
from app import models

router = APIRouter()


@router.post("/{myth_id}/post-comment", response_model=schemas.CommentPost)
def post_comment(comment: schemas.CommentPost, myth_id: uuid.UUID, db: Session = Depends(get_db),
                 current_user=Depends(utils.get_current_active_user)):
    db_myth = crud.get_myth_by_id(db=db, uuid=myth_id)
    if not db_myth:
        raise HTTPException(status_code=404, detail="Myth not found")
    if comment.parent_id:
        db_parent_comment = crud.get_comment_by_id(db=db, id=comment.parent_id)
        if not db_parent_comment:
            raise HTTPException(status_code=404, detail="Comment not found")
    return crud.post_comment(comment=comment, db=db, author_id=current_user.id, post_id=myth_id)


@router.get("/{myth_id}/get-comments", response_model=List[schemas.CommentRead])
def get_comments_by_post(myth_id: uuid.UUID, db: Session = Depends(get_db),
                         current_user=Depends(utils.get_current_active_user)):
    db_myth = crud.get_myth_by_id(db=db, uuid=myth_id)
    if not db_myth:
        raise HTTPException(status_code=404, detail="Myth not found")
    comments = crud.get_all_comments_from_myth(db, myth_id)
    return comments
#idk somethign to commit really
