import os

import uuid
from fastapi import APIRouter, HTTPException, status
from datetime import timedelta
from app.db import get_db
from fastapi import Depends
from sqlalchemy.orm import Session
from app import crud
import dotenv
from app import schemas
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from app import utils

dotenv.load_dotenv()
router = APIRouter()


@router.post("/token", response_model=utils.Token)
def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = utils.authenticate_user(form_data.username, form_data.password, db=db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password',
            headers={'WWW-Authenticate': 'Bearer'}
        )
    print(os.environ.get("ACCESS_TOKEN_EXPIRES_MINUTES"))
    access_token_expires = timedelta(minutes=int(os.environ.get("ACCESS_TOKEN_EXPIRE_MINUTES")))
    access_token = utils.create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@router.get("/get-all-users")
def get_all_users(db: Session = Depends(get_db), current_user=Depends(utils.get_current_user)):
    if current_user.isAdmin:
        return crud.get_all_users(db=db)
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="You have no power here")


@router.post("/post-user")
def post_user(user: schemas.UserPost, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_name(db=db, user_name=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="User already exists")
    return crud.post_user(db=db, user=user)


@router.patch('/change-password')
def change_user_password(current_password: str, new_password, current_user=Depends(utils.get_current_user),
                         db: Session = Depends(get_db)):
    if utils.verify_password(current_password, current_user.password):
        return crud.change_user_password(db=db, user_id=current_user.id, password=new_password)
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Wrong password")


@router.post("/create-admin-user")
def create_admin_user(user: schemas.UserPost, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_name(db=db, user_name=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="User already exists")
    return crud.post_user(db=db, user=user, admin=True)


@router.get("/users/me")
def read_user_me(current_user=Depends(utils.get_current_user)):
    return current_user


@router.delete("/delete-user/{user_id}")
def delete_user(user_id: uuid.UUID, db: Session = Depends(get_db), current_user=Depends(utils.get_current_user)):
    if current_user.isAdmin:
        db_user = crud.get_user(db=db, user_id=user_id)
        if not db_user:
            raise HTTPException(status_code=404, detail="User not found")
        elif db_user.isAdmin:
            raise HTTPException(status_code=400, detail="You cannot fell the admin ")
        return crud.delete_user(db=db, user_id=user_id)
    else:
        raise HTTPException(status_code=400,
                            detail="Begone foul demon,authority has not been given to you to deny the existence of this user")


@router.patch('/update-user')
def update_user(user: schemas.UserBase, db=Depends(get_db), current_user=Depends(utils.get_current_user)):
    return crud.update_user(db=db, user=user, user_id=current_user.id)
