import datetime

from fastapi import APIRouter, Depends, HTTPException, File, UploadFile
from sqlalchemy.orm import Session
import dotenv
from fastapi.responses import FileResponse
from app.db import get_db
import uuid
import shutil
import os
from app import crud, schemas

dotenv.load_dotenv()
router = APIRouter()


@router.post("/{character_id}/post-photo")
def post_photo(character_id: uuid.UUID, uploaded_file: UploadFile = File(...), db: Session = Depends(get_db)):
    db_character = crud.get_characters(db=db, character_id=character_id)
    if db_character:
        url = os.path.join(os.environ.get("MEDIA_URI"), f"{db_character.name}/")
        name = f"{datetime.datetime.now()}.png"
        photo = schemas.PhotoWrite(character_id=character_id)
        uploaded_file.filename = name
        if not os.path.exists(url):
            os.mkdir(url, 0o0755)
        with open(url + uploaded_file.filename, "wb") as image:
            shutil.copyfileobj(uploaded_file.file, image)
            url = os.path.join(url, name)
        return crud.post_photo(db=db, photo=photo, url=url, name=name)
    else:
        raise HTTPException(status_code=404, detail="Character not found")


@router.get("/{character_id}/download-photo")
def download_photo_by_character_id(character_id: uuid.UUID, db=Depends(get_db)):
    db_character = crud.get_characters(db=db, character_id=character_id)
    if not db_character:
        raise HTTPException(status_code=404, detail="Character not found")

    db_photo = crud.get_photo(db=db, photo_id=db_character.photo[0].id)
    if not db_photo:
        raise HTTPException(status_code=404, details="character doesnt have a photo")

    return FileResponse(path=db_photo.url, media_type='image/jpeg', filename=db_photo.name)


@router.get("/{character_id}/get-photo")
def get_photo_by_character_id(character_id: uuid.UUID, db=Depends(get_db)):
    db_character = crud.get_characters(db=db, character_id=character_id)
    if not db_character:
        raise HTTPException(status_code=404, detail="Character not found")

    db_photo = crud.get_photo(db=db, photo_id=db_character.photo[0].id)
    if not db_photo:
        raise HTTPException(status_code=404, details="character doesnt have a photo")
    return db_photo
