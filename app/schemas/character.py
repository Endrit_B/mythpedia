import uuid
from typing import Optional

from pydantic import BaseModel

from .myths import MythWrite, MythTitle, MythTest


class CharacterWrite(BaseModel):
    name: str
    origin: str
    myth_title: str

    class Config:
        orm_mode = True


class CharacterRead(CharacterWrite):
    id: uuid.UUID

    class Config:
        orm_mode = True


class CharacterWriteWithMyth(BaseModel):
    name: str
    origin: str
    myth: MythWrite

    class Config:
        orm_mode = True


class CharacterDelete(BaseModel):
    id: uuid.UUID
    name: str
    origin: str

    class Config:
        orm_mode = True
