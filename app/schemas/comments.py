import uuid

from pydantic import BaseModel
from typing import Optional, List
import datetime
from app import models


class CommentsBase(BaseModel):
    content: str


    class Config:
        orm_mode = True


class CommentPost(CommentsBase):
    parent_id: Optional[int] = None

    class Config:
        orn_mode = True


class CommentRead(CommentsBase):
    replies: List["CommentRead"]
    id: int
    parent_id: Optional[int]
    datetime: datetime.datetime
    class Config:
        orm_mode = True
