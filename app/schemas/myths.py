import uuid
from typing import Optional

from pydantic import BaseModel


class MythWrite(BaseModel):
    id: uuid.UUID
    title: str
    content: str

    class Config:
        orm_mode = True


class MythTest(BaseModel):
    id: uuid.UUID

    class Config:
        orm_mode = True


class MythUpdate(BaseModel):
    title: Optional[str]
    content: Optional[str]

    class Config:
        orm_mode = True


class MythTitle(BaseModel):
    title: str
