from pydantic import BaseModel
import uuid


class UserBase(BaseModel):
    fullname: str
    username: str

    class Config:
        orm_mode = True


class UserPost(UserBase):
    password: str
    email: str

    class Config:
        orm_mode = True


class UserRead(UserPost):
    id: uuid.UUID

    class Config:
        orm_mode = True
