from pydantic import BaseModel
import uuid
from datetime import datetime


class PhotoBase(BaseModel):


    class Config:
        orm_mode = True


class PhotoWrite(PhotoBase):
    character_id: uuid.UUID

    class Config:
        orm_mode = True


class PhotoRead(PhotoWrite):
    id: uuid.UUID
    upload_time: datetime
    url: str
    name: str
    class Config:
        orm_mode = True
