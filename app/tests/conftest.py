import os
from contextlib import contextmanager
import pytest
from sqlalchemy.orm import close_all_sessions
from sqlalchemy import create_engine
from app.db import Base, get_db
from sqlalchemy.orm import sessionmaker
from ..main import app
import dotenv

dotenv.load_dotenv()
DATABASE_URI = os.environ.get("TEST_DATABASE_URI",'postgresql://postgres:15551912@localhost/mythpedia_test')

engine = create_engine(
    DATABASE_URI, pool_size=20, max_overflow=20, echo=True
)
Testing_session_local = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)


def override_get_db():  # pragma: no cover (not used in tests)
    session = Testing_session_local()
    try:
        yield session
    finally:
        session.close()


app.dependency_overrides[get_db] = override_get_db


@contextmanager
def rollback_on_exception(db):
    try:
        yield db
    except Exception:
        db.rollback()
        raise


@pytest.fixture()
def test_db():
    close_all_sessions()
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)


@pytest.fixture()
def db_test_session():
    session = Testing_session_local()
    try:
        yield session
    finally:
        session.close()
