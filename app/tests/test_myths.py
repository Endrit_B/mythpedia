from fastapi.testclient import TestClient
from app.main import app
from app import crud
import json
import uuid
from app import schemas

client = TestClient(app)
dummy_myth = json.dumps({"id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                         "title": "this is a test title ",
                         "content": "this is a test content , honestly i dont know what else to write here.Im not sure how the text mapping to string ordeal works"},
                        )


def test_post_myths(test_db, db_test_session):
    response = client.post("/post-myth",
                           json={"id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                                 "title": "this is a test title ",
                                 "content": "this is a test content , honestly i dont know what else to write here.Im not sure how the text mapping to string ordeal works"}

                           )
    assert response.status_code == 200
    crud_myth = crud.get_myth_by_id(db=db_test_session, uuid=uuid.UUID("3fa85f64-5717-4562-b3fc-2c963f66afa6"))
    assert str(crud_myth.id) == response.json().get("id")
    assert crud_myth.title == response.json().get("title")
    assert crud_myth.content == response.json().get("content")


def test_get_myth(test_db, db_test_session):
    crud.post_myth(db=db_test_session,
                   myth=schemas.MythWrite(id="3fa85f64-5717-4562-b3fc-2c963f66afa6", title="this is a test title",
                                          content="this is a test content , honestly i dont know what else to write here.Im not sure how the text mapping to string ordeal works"))
    response = client.get("/get-myth?id=3fa85f64-5717-4562-b3fc-2c963f66afa6"

                          )
    assert response.status_code == 200
    assert response.json().get("id") == "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    assert response.json().get("title") == "this is a test title"
    assert response.json().get(
        "content") == "this is a test content , honestly i dont know what else to write here.Im not sure how the text mapping to string ordeal works"


def test_get_all_myths(test_db, db_test_session):
    crud.post_myth(db=db_test_session,
                   myth=schemas.MythWrite(id="3fa85f64-5717-4562-b3fc-2c963f66afa6", title="this is a test title",
                                          content="this is a test content , honestly i dont know what else to write here.Im not sure how the text mapping to string ordeal works"))

    crud.post_myth(db=db_test_session,
                   myth=schemas.MythWrite(id="3fa85f64-5717-4562-b3fc-1c363f66afa6", title="this is a test title2",
                                          content="this is a test content , honestly i dont know what else to write here.Im not sure how the text mapping to string ordeal works"))

    response = client.get("/get-all-myths")
    assert response.status_code == 200


def test_delete_myth(test_db, db_test_session):
    crud.post_myth(db=db_test_session,
                   myth=schemas.MythWrite(id="3fa85f64-5717-4562-b3fc-2c963f66afa6", title="this is a test title",
                                          content="this is a test content , honestly i dont know what else to write here.Im not sure how the text mapping to string ordeal works"))

    response = client.delete("delete-myth?myth_id=3fa85f64-5717-4562-b3fc-2c963f66afa6")
    assert response.status_code == 200
    db_myth = crud.get_myth_by_id(db=db_test_session, uuid=uuid.UUID("3fa85f64-5717-4562-b3fc-2c963f66afa6"))
    assert db_myth is None


def test_update_myth(test_db, db_test_session):
    crud.post_myth(db=db_test_session,
                   myth=schemas.MythWrite(id="3fa85f64-5717-4562-b3fc-2c963f66afa6", title="this is a test title",
                                          content="this is a test content , honestly i dont know what else to write here.Im not sure how the text mapping to string ordeal works"))

    response = client.patch("/update-myth/3fa85f64-5717-4562-b3fc-2c963f66afa6", json={
        "title": "updated title",
        "content": "updated content"

    })
    assert response.status_code == 200
    db_myth = crud.get_myth_by_id(db=db_test_session, uuid=uuid.UUID("3fa85f64-5717-4562-b3fc-2c963f66afa6"))
    assert db_myth.title == "updated title"
    assert db_myth.content == "updated content"
