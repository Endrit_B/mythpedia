from fastapi.testclient import TestClient
from app.main import app
from app import crud
import json
import uuid
from app import schemas

client = TestClient(app)


def test_post_character(test_db, db_test_session):
    response = client.post("/post-myth",
                           json={"id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                                 "title": "Elden Ring",
                                 "content": "this is a test content , honestly i dont know what else to write here.Im not sure how the text mapping to string ordeal works"}

                           )
    assert response.status_code == 200
    response = client.post("/character", json=
    {
        "name": "Radagon",
        "origin": "The Lands Between",
        "myth_title": "Elden Ring"
    }
                           )

    assert response.status_code == 201
    db_character = crud.get_character_by_name(db=db_test_session, name="Radagon")
    assert db_character.name == "Radagon"
    assert db_character.origin == "The Lands Between"
    assert db_character.myth_id == uuid.UUID("3fa85f64-5717-4562-b3fc-2c963f66afa6")


def test_get_character(test_db, db_test_session):
    client.post("/post-myth",
                json={"id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                      "title": "Elden Ring",
                      "content": "this is a test content , honestly i dont know what else to write here.Im not sure how the text mapping to string ordeal works"}

                )
    client.post("/character", json={
        "name": "Radagon",
        "origin": "The Lands Between",
        "myth_title": "Elden Ring"
    }
                )
    db_char = crud.get_character_by_name(db=db_test_session, name="Radagon")
    response = client.get(f"/get-character?character_id={db_char.id}")
    assert response.status_code == 200


def test_delete_character(test_db, db_test_session):
    client.post("/post-myth",
                json={"id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                      "title": "Elden Ring",
                      "content": "this is a test content , honestly i dont know what else to write here.Im not sure how the text mapping to string ordeal works"}

                )
    client.post("/character", json={
        "name": "Radagon",
        "origin": "The Lands Between",
        "myth_title": "Elden Ring"
    }
                )
    db_char = crud.get_character_by_name(db=db_test_session, name="Radagon")
    response = client.delete(f"/delete-character/{db_char.id}")
    assert response.status_code == 200
