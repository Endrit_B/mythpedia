import os
from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import dotenv
dotenv.load_dotenv()

DATABASE_URI = os.environ.get("DATABASE_URI")

engine = create_engine(
    DATABASE_URI, pool_size=20, max_overflow=20, echo=True
)
session_factory = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():  # pragma: no cover (not used in tests)
    session = session_factory()
    try:
        yield session
    finally:
        session.close()


@contextmanager
def rollback_on_exception(db):
    try:
        yield db
    except Exception:
        db.rollback()
        raise
