import uuid

from sqlalchemy import Column, ForeignKey, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import Relationship
from sqlalchemy_utils import URLType
from app.db import Base


class Character(Base):
    __tablename__ = "characters"
    id: uuid.UUID = Column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid.uuid4())
    name = Column(String(length=255))
    origin = Column(String(length=255))
    myth_id: uuid.UUID = Column(UUID(as_uuid=True), ForeignKey("myths.id"))
    myth = Relationship("Myths", back_populates="characters")
    photo = Relationship("Photo",back_populates="character")
