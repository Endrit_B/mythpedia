import uuid

from sqlalchemy.orm import Relationship

from app.db import Base
from sqlalchemy import Column, String, Boolean
from sqlalchemy.dialects.postgresql import UUID


class User(Base):
    __tablename__ = "users"
    id: uuid.UUID = Column(UUID(as_uuid=True), index=True, default=uuid.uuid4, primary_key=True)
    fullname: str = Column(String(length=255), nullable=False)
    username: str = Column(String(length=255, ), nullable=False)
    password: str = Column(String(length=255, ), nullable=False)
    disabled: bool = Column(Boolean, default=False)
    email: str = Column(String(length=255, ), nullable=False)
    isAdmin: bool = Column(Boolean, default=False)
    comments = Relationship("Comment", back_populates="author")
