from sqlalchemy.orm import Relationship

from app.db import Base
import uuid
from sqlalchemy import Column, Integer, String, Text, DateTime, UUID, ForeignKey
from datetime import datetime
from sqlalchemy.sql import func


class Comment(Base):
    __tablename__ = "comments"
    id: int = Column(Integer, primary_key=True, autoincrement=True)
    content: str = Column(Text)
    datetime: datetime = Column(DateTime(timezone=True), server_default=func.now())
    parent_myth_id: uuid.UUID = Column(UUID(as_uuid=True), ForeignKey("myths.id", ondelete='CASCADE'))
    parent_myth = Relationship("Myths", back_populates="comments", )
    author = Relationship("User", back_populates="comments")
    author_id: uuid.UUID = Column(UUID(as_uuid=True), ForeignKey("users.id", ondelete='SET NULL'))
    parent_id: int = Column(Integer, ForeignKey("comments.id"))
    # parent = Relationship("Comment", remote_side=[id])
    replies = Relationship("Comment", remote_side=[parent_id])
