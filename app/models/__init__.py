from .character import Character
from .myths import Myths
from .photo import Photo
from.users import User
from .comments import Comment
__all__ = ["Character", "Myths",Comment,"Photo","User"]
