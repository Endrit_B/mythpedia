from app.db import Base
from sqlalchemy import Column, String, DateTime, ForeignKey
from sqlalchemy.orm import Relationship
from sqlalchemy_utils import URLType
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql import func
import uuid


class Photo(Base):
    __tablename__ ="photos"
    id: uuid.UUID = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4(), index=True)
    name: str = Column(String(length=255))
    url = Column(String(length=255))
    upload_time = Column(DateTime(timezone=True), server_default=func.now())
    character_id :uuid.UUID = Column(UUID(as_uuid=True), ForeignKey("characters.id"))
    character=Relationship("Character",back_populates="photo")
