import uuid

from sqlalchemy import Column, String, Text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import Relationship

from app.db import Base


class Myths(Base):
    __tablename__ = "myths"
    id: uuid = Column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid.uuid4())
    title = Column(String(length=255))
    characters = Relationship("Character", back_populates="myth")
    content = Column(Text)
    comments = Relationship("Comment", back_populates="parent_myth",passive_deletes=True)
