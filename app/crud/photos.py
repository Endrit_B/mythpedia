import uuid
from app import models, schemas
from sqlalchemy.orm import Session


def post_photo(photo: schemas.PhotoWrite, db: Session, url: str, name: str):
    db_photo = models.Photo(character_id=photo.character_id, url=url, name=name)
    db.add(db_photo)
    db.commit()
    db.refresh(db_photo)
    return db_photo


def get_photo(photo_id: uuid.UUID, db: Session):
    return db.query(models.Photo).filter(models.Photo.id == photo_id).first()
