import uuid
from fastapi import HTTPException
from sqlalchemy.orm import Session
from app import models
from app import schemas
from app import utils


def get_user(db: Session, user_id):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_all_users(db: Session):
    return db.query(models.User).all()


def get_user_by_name(db: Session, user_name: str):
    return db.query(models.User).filter(models.User.username == user_name).first()


def post_user(db: Session, user: schemas.UserPost, admin: bool = False):
    hashed_password = utils.get_password_hash(user.password)
    db_user = models.User(fullname=user.fullname, username=user.username, email=user.email,
                          password=hashed_password, isAdmin=admin)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def delete_user(db: Session, user_id: uuid.UUID):
    db.query(models.User).filter(models.User.id == user_id).delete()
    db.commit()
    return "User eradicated"


def change_user_password(db: Session, user_id: uuid.UUID, password: str):
    hashed_password = utils.get_password_hash(password)
    db.query(models.User).filter(models.User.id == user_id).update({'password': hashed_password})
    db.commit()
    return "Password changed successfully"


def update_user(user: schemas.UserBase, db: Session, user_id: uuid.UUID):
    db_user = get_user_by_name(db=db, user_name=user.fullname)
    if db_user:
        raise HTTPException(status_code=409, detail="such name already exists")
    else:
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print(user.dict())
        db.query(models.User).filter(models.User.id == user_id).update(
            user.dict())
        db.commit()
    return "user updated"
