import uuid

from sqlalchemy.orm import Session
from sqlalchemy import select
from fastapi import HTTPException

from app import models, schemas


def get_character_by_name(name: str, db: Session):
    return db.query(models.Character).filter(models.Character.name == name).first()


def get_character_by_id(id: uuid.UUID, db: Session):
    return db.query(models.Character).filter(models.Character.id == id).first()


def get_characters(db: Session, character_id: uuid.UUID):
    return (
        db.query(models.Character)
        .filter(models.Character.id == character_id)
        .first()
    )


def post_character(db: Session, character: schemas.CharacterWrite):
    # query = select(models.Myths).where(models.Myths.title == character.myth_title)
    db_myth = db.query(models.Myths).filter(models.Myths.title == character.myth_title).first()
    if db_myth:

        db_character = models.Character(name=character.name, origin=character.origin,
                                        myth_id=db_myth.id)

        db.add(db_character)
        db.flush()
        db.commit()
        db.refresh(db_character)
        return db_character
    else:
        raise HTTPException(status_code=404 , detail="A myth with that title was not found")


def delete_character(db: Session, uuid: uuid.UUID):
    db_character = get_character_by_id(db=db, id=uuid)
    db.delete(db_character)
    db.commit()
    return db_character
