from .character import get_characters, post_character, delete_character, get_character_by_name
from .myths import get_all_myths, get_myth_by_id, post_myth, delete_myth, update_myth, get_myth_byt_tittle
from .photos import post_photo, get_photo
from .users import get_user, get_all_users, get_user_by_name, post_user, delete_user, change_user_password, update_user
from .comments import post_comment, get_comment_by_id, get_all_comments_from_myth

__all__ = ["get_characters", "post_character", "delete_character", "get_myth_by_id", "get_all_myths", "post_myth",
           "delete_myth", "update_myth", "get_character_by_name", "get_myth_byt_tittle", "post_photo", "get_photo",
           "get_user", "get_all_users", "get_user_by_name", "get_all_comments_from_myth", "post_user", "delete_user",
           "change_user_password",
           "update_user", "get_comment_by_id", "post_comment"]
