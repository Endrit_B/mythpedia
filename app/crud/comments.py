import uuid

from app import schemas
from sqlalchemy.orm import Session
from app import models


def post_comment(db: Session, comment: schemas.CommentPost, author_id: uuid.UUID, post_id: uuid.UUID):
    db_comment = models.Comment(content=comment.content, parent_id=comment.parent_id,
                                parent_myth_id=post_id, author_id=author_id)
    db.add(db_comment)
    db.commit()
    db.refresh(db_comment)
    return db_comment


def get_all_comments_from_myth(db: Session, post_id: uuid.UUID):
    comments = db.query(models.Comment).filter(models.Comment.parent_myth_id == post_id,
                                               models.Comment.parent_id == None).all()
    return comments


def get_comment_by_id(db: Session, id: int):
    return db.query(models.Comment).filter(models.Comment.id == id).first()
