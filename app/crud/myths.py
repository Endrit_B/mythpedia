import uuid

from sqlalchemy import select, update
from sqlalchemy.orm import Session

from app import models, schemas


def get_myth_by_id(db: Session, uuid: uuid.UUID):
    return db.query(models.Myths).filter(models.Myths.id == uuid).first()


def get_myth_byt_tittle(db: Session, title: str):
    return db.query(models.Myths).filter(models.Myths.title == title).first()


def get_all_myths(db: Session):
    return db.query(models.Myths).all()


def post_myth(db, myth: schemas.MythWrite):
    db_myth = models.Myths(title=myth.title, id=myth.id,content=myth.content)
    db.add(db_myth)
    db.commit()
    db.refresh(db_myth)
    return db_myth


def delete_myth(db: Session, id: uuid.UUID):
    db_myth = get_myth_by_id(db, id)
    db.delete(db_myth)
    db.commit()
    return db_myth


def update_myth(myth_id: uuid.UUID, myth: schemas.MythUpdate, db: Session):
    data = myth.dict()
    query = (update(models.Myths).where(models.Myths.id == myth_id).values(data))
    db.execute(query)
    db.commit()
    return db.query(models.Myths).filter(models.Myths.id == myth_id).first()
