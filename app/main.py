from fastapi import APIRouter, Depends, FastAPI
import uvicorn
from app.routers import character, myths, photos, users, auth, comments
from fastapi.staticfiles import StaticFiles

app = FastAPI()

app.include_router(character.router)
app.include_router(myths.router)
app.include_router(photos.router)
app.include_router(users.router)
app.include_router(comments.router)
app.mount("/static", StaticFiles(directory="Media"), name="static")
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000, reload=True)
